package com.spring.h2dbexample.springbooth2dbexample;

import com.spring.h2dbexample.springbooth2dbexample.model.Product;
import com.spring.h2dbexample.springbooth2dbexample.service.ProductService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ComponentScan(basePackages = "com.spring.h2dbexample.springbooth2dbexample")
public class SpringBootH2dbExampleApplicationTests {

	@Autowired
	ProductService productService;

	@Test
	public void contextLoads() {
	}

	@Test
	public void createProductTest(){
		Product product = Product.builder()
				.name("test")
				.brandName("test2")
				.price(2900)
				.size("9")
				.productType("shoe")
				.productColor("black")
				.build();

		Product product1 = productService.createProduct(product);
		Assert.assertNotNull(product1.getId());
	}

}
