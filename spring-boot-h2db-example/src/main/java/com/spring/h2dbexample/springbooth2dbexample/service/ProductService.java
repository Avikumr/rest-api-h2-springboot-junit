package com.spring.h2dbexample.springbooth2dbexample.service;

import com.spring.h2dbexample.springbooth2dbexample.model.Product;
import com.spring.h2dbexample.springbooth2dbexample.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<Product> findAll(){
        return productRepository.findAll();
    }

    public Optional<Product> findById(UUID uuid){
        if(null==uuid){
            return Optional.empty();
        }else{
            return productRepository.findById(uuid);
        }
    }


    public Product createProduct(Product product){
        System.out.println("Product In service Layer : "+product);
        if(validateProduct(product)){
            productRepository.save(product);
            System.out.println(" 333333 : "+product);
            return productRepository.getOne(product.getId());
        }else{
            return null;
        }
    }

    public boolean validateProduct(Product product){
        if(null!=product && null!=product.getName() && null!=product.getPrice()){
            return true;
        }else{
            return false;
        }
    }
}
