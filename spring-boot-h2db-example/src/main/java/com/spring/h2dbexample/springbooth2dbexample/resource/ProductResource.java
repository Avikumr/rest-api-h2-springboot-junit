package com.spring.h2dbexample.springbooth2dbexample.resource;

import com.spring.h2dbexample.springbooth2dbexample.model.Product;
import com.spring.h2dbexample.springbooth2dbexample.repository.ProductRepository;
import com.spring.h2dbexample.springbooth2dbexample.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;


@RestController
@RequestMapping(value = "/products")
public class ProductResource {

    protected static final Logger logger = LoggerFactory.getLogger(ProductResource.class);

    @Autowired
    ProductService productService;

    @GetMapping(value = "/all")
    public List<Product> getAll(){
        return productService.findAll();
    }

    @GetMapping(value = "/get/{id}")
    public Optional<Product> getProductById(@PathVariable("id") UUID uuid){
        logger.info("Find product by id: {}", uuid);
        return productService.findById(uuid);
    }

    @PostMapping(value = "/create")
    public Product persist(@RequestBody final Product product){
        logger.info("Create product");
        return productService.createProduct(product);
    }
}
