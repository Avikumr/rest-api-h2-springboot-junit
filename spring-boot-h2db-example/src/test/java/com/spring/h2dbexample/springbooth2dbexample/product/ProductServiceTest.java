package com.spring.h2dbexample.springbooth2dbexample.product;

import com.spring.h2dbexample.springbooth2dbexample.model.Product;
import com.spring.h2dbexample.springbooth2dbexample.service.ProductService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
@SpringBootTest
@ComponentScan(basePackages = "com.spring.h2dbexample.*")
public class ProductServiceTest {

    @Autowired
    ProductService productService;

    @Before
    public  void init(){
        //Do nothing as of now
        Product product = Product.builder()
                .name("test")
                .brandName("test2")
                .price(2900)
                .size("9")
                .productType("shoe")
                .productColor("black")
                .build();

        Product product1 = productService.createProduct(product);
    }

    @Test
    public void createProductTest(){
        Product product = Product.builder()
                .name("test")
                .brandName("test2")
                .price(2900)
                .size("9")
                .productType("shoe")
                .productColor("black")
                .build();

        Product product1 = productService.createProduct(product);
        Assert.assertNotNull(product1.getId());
    }


    @Test
    public void testGetProduct(){
        List<Product> list = productService.findAll();
        Assert.assertNotNull(list);
        Assert.assertEquals(list.size(), 1);
    }
}
