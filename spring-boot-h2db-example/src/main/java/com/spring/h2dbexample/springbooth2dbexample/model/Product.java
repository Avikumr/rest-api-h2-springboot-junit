package com.spring.h2dbexample.springbooth2dbexample.model;

import jdk.nashorn.internal.objects.annotations.Getter;
import lombok.Builder;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Builder
public class Product {

    @Id
    @Column(name= "id", columnDefinition = "uuid")
    @GeneratedValue(strategy = GenerationType.AUTO)
//    @GeneratedValue(generator = "uuid2")
//    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    private UUID id;

    @Column(name= "name")
    private String name;

    @Column(name= "brandName")
    private String brandName;

    @Column(name= "productType")
    private String productType;

    @Column(name= "price")
    private Integer price;

    @Column(name= "size")
    private String size;

    @Column(name= "productColor")
    private String productColor;
}
