package com.spring.h2dbexample.springbooth2dbexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = "com.spring.h2dbexample.springbooth2dbexample.repository")
//@EnableAutoConfiguration
//@ComponentScan
public class SpringBootH2dbExampleApplication {


	public static void main(String[] args) {
		SpringApplication.run(SpringBootH2dbExampleApplication.class, args);
	}


}
